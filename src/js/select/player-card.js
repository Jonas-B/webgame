import { asset } from '../config';

var Card = function(player){
	this.props = {};
	this.container = document.createElement('div');
	this.imgWrap = document.createElement('div');
	this.contentWrap = document.createElement('div');
	this.propsWrap = document.createElement('div');
	this.detailWrap = document.createElement('div');
	this.picked = document.createElement('div');

	this.img = document.createElement('img');
	this.name = document.createElement('h2');
	this.description = document.createElement('div');
	this.btn = document.createElement('button');

	this.imgWrap.appendChild(this.img);
	this.detailWrap.appendChild(this.description)
	this.detailWrap.appendChild(this.btn);
	this.contentWrap.appendChild(this.name)
	this.contentWrap.appendChild(this.propsWrap)
	this.contentWrap.appendChild(this.detailWrap);
	this.container.appendChild(this.imgWrap)
	this.container.appendChild(this.contentWrap);
	this.container.appendChild(this.picked);

	this.style = function(){
		this.container.className = 'player-card';
		this.imgWrap.className = 'card-img';
		this.contentWrap.className = 'card-text';
		this.propsWrap.className = 'props';
		this.detailWrap.className = 'detail';
		this.picked.className = 'picked';

		this.name.className = 'name';
		this.description.className = 'description';
		this.btn.className = 'play';
	}

	this.staticContent = function(){
		this.btn.innerHTML = '<i class="material-icons">play_circle_filled</i> Play !';
		this.name.innerHTML = player.name;
		this.description.innerHTML = player.description;
		this.img.src = player.src;
		this.picked.innerHTML = player.picked;
	}

	this.makeProps = function(){
		var props = { hp: "favorite", speed: "arrow_forward", jump: "arrow_upward" };
		for(var prop in props){
			if(player[prop] != undefined){
				this.props[prop] = player[prop];
				var propWrap = document.createElement('div');
				propWrap.className = 'prop '+prop;

				var propIcon = document.createElement('div');
				propIcon.className = 'ico material-icons';
				propIcon.innerHTML = props[prop];

				var propValue = document.createElement('div');
				propValue.className = 'value';

				propWrap.appendChild(propIcon)
				propWrap.appendChild(propValue);

				this.propsWrap.appendChild(propWrap)
			}
		}
	}

	this.style();
	this.staticContent();
	this.makeProps();

	this.propStrength = function(best){
		for(var prop in best){
			var node = this.propsWrap.querySelector('.prop.'+prop+' .value');
			var percent = Math.round(this.props[prop]/best[prop]*100);
			node.style.width = percent+'%';
		}
	}
}

module.exports = Card;