import { asset } from './config';
import ajax from './core/loader/ajax';

import Game from './core/Game';
import Card from './select/player-card';
import Player from './core/ui/player/Player';

NodeList.prototype.forEach = Array.prototype.forEach;

var container = document.querySelector('.container');
var gamecontainer = document.querySelector('.game');
var cv = document.querySelector('#game-canvas');
var ctx = cv.getContext('2d');
var game = new Game(cv);
var selector = document.querySelector('.player-select');

var printCards = function(chars){
	var cards = [];
	var bestCaract = {
		hp: 0,
		speed: 0,
		jump: 0
	}

	chars.forEach(function(char){
		var card = new Card(char);
		selector.appendChild(card.container);
		cards.push(card);

		for(var carac in bestCaract){
			if(bestCaract[carac] < char[carac]){
				bestCaract[carac] = char[carac];
			}
		}
	});

	cards.forEach(function(card){
		card.propStrength(bestCaract);
	});
}

ajax({
	url: asset.build('/chars/chars.json'),
	response: 'json',
	cb: function(data){
		var characters = game.getPlayers(data);

		printCards(characters);

		var menu = document.querySelector('.menu .hamburger');
		var options = document.querySelector('.menu .options')
		menu.addEventListener('click', function(){
			menu.classList.toggle('is-active')
			options.classList.toggle('opened')
		})

		var windowHeight = window.innerHeight;
		var windowWidth = window.innerWidth;
		var windowMax = windowWidth < windowHeight ? windowHeight : windowWidth;

		var cards = document.querySelectorAll('.player-card');
		cards.forEach(function(card, cardIndex){
			card.addEventListener('click', function(e){
				var selected = document.querySelector('.player-card.selected');
				if(selected) selected.classList.remove('selected');
				card.classList.add('selected');
			})

			var playBtn = card.querySelector('.play');
			playBtn.addEventListener('click', function(e){
				var x = e.pageX;
				var y = e.pageY;

				var t = document.createElement('div');
				t.className = 'game-transition';
				t.style.left = x+'px';
				t.style.top = y+'px';

				var m = document.createElement('div');
				m.className = 'game-transition-msg';
				m.innerHTML = '<p><h1> You picked <strong>'+card.querySelector('.name').textContent+'</strong> </h1></p><p>'+card.querySelector('.picked').textContent+'</p>';
				t.append(m)

				document.body.append(t);
				setTimeout(function(){
					t.style.left = '-'+(windowMax/2)+'px';
					t.style.width = (2*windowMax)+'px';

					t.style.top = '-'+(windowMax/2)+'px';
					t.style.height = (2*windowMax)+'px';

					setTimeout(function(){
						container.style.display = 'none';
						gamecontainer.style.display = 'flex';
						cv.height = 1080;
						cv.width = 1920;

					 	game.init(characters[cardIndex], function(){
					 		game.print();
					 		t.style.opacity = '0';
					 		game.run();
					 	});
					}, 1000)
				}, 10)
			})
		})
	}
})