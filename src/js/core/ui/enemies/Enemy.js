import Printer from '../Printer';

class Player extends Printer{
	constructor(ctx){
		super(ctx);

		this.x = 50;
		this.y = 400;

		this.label = 'Enemy';

		this.printed = false;

		this.width = 0;
		this.height = 0;
		this.moving = false;

		this.step = 0;
		this.defaultState = "run";
		this.state = {
			default: "run",
			current: "run",
			locked: false
		}

		this.states = {
			run: {
				target: 0,
				frame: 0,
				step: 0
			},
			jump: {
				target: 0,
				frame: 0,
				step: 0
			},
			sneak: {
				target: 0,
				frame: 0,
				step: 0
			}
		}

		this.speed = 0;

		this.img = new Image();
		this.ressource = this.img;
	}

	frames(){
		return Math.round(this.img.width/this.width)-1;
	}

	move(){
		if(this.printed){
			this.printed = true;
		}
	}
}

module.exports = Player;