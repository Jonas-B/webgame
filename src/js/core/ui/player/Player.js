import { asset } from '../../../config';
import Printer from '../Printer';

class Player extends Printer{
	constructor(ctx, opt = {}){
		super(ctx);

		this.default = {
			name: "Unknown name",
			description: "Unknown description",
			picked: "Good choice !",
			hp: 1,

			x: 50,
			y: 200,

			label: 'Avatar',

			width: 0,
			height: 0,

			speed: 0,
			jump: 50,

			src: 'chars/img/undefined.png'
		}

		for(var key in this.default){
			this[key] = opt[key] || this.default[key];
		}

		this.src = asset.build(this.src);

		this.jumpTime = 1;
		this.basey = this.y;
		this.moving = false;

		this.defaultState = "run";
		this.state = {
			default: "run",
			current: "run",
			locked: false
		}

		opt.states = opt.states || {};
		opt.states.run = opt.states.run || {};
		opt.states.jump = opt.states.jump || {};
		opt.states.sneak = opt.states.sneak || {};

		this.states = {
			run: {
				target: opt.states.run.target || 0,
				frame: 0,
				step: 0
			},
			jump: {
				target: opt.states.jump.target || 0,
				frame: 0,
				step: 0,
				time: 0
			},
			sneak: {
				target: opt.states.sneak.target || 0,
				frame: 0,
				step: 0
			}
		}

		this.img = new Image();
		this.ressource = this.img;

		console.log(this.ctx);
	}

	frames(){
		return Math.round(this.img.width/this.width)-1;
	}

	move(){
		var state = this.state.current;
		var stateData = this.states[state];

		this.ctx.drawImage(this.img, stateData.frame, stateData.target, this.width, this.height, this.x, this.y, this.width, this.height);
		
		if(!this.moving){
			if(this.state.locked){
				if(this.state.current == 'jump'){
					if(stateData.step == this.frames()){
						this.state.locked = false;
					}
				}
				else{
					this.state.locked = false;
				}
			}
			if(this.state.current == 'jump'){
				var jumpSteps = this.speed * this.jumpTime;
				var half = jumpSteps/2;
				var mult = Math.sqrt(this.jump)/half;
				var xstep = (stateData.time-half) * mult;
				var jumpRatio = xstep * (-xstep) + this.jump;
				this.y = this.basey - jumpRatio;
				stateData.time += 1;
				var tick = jumpSteps/this.frames();
				stateData.step += stateData.time % tick == 0 ? 1 : 0;
			}
			else{
				stateData.step += 1;
			}

			this.moving = true;
			this.reset = setTimeout(function(){
				stateData.frame = stateData.step*this.width;
				if(this.state.current == state){
					if(stateData.frame >= this.img.width) {
						stateData.frame = 0;
						stateData.step = 0;
						stateData.time = 0;
						if(this.state.current == 'jump'){
							this.state.current = this.state.default;
						}
					}
				}
				this.moving = false;
			}.bind(this), 1000/this.speed)
		}
	}

	updateState(state){
		if(this.state.current != state){
			if(!this.state.locked){
				console.log('State update.', state)

				this.states[this.state.current].step = 0;
				this.states[this.state.current].frame = 0;

				this.state.current = state;
				this.state.locked = true;

				this.states[state].step = 0;
				this.states[state].frame = 0;
			}
		}
	}
}

module.exports = Player;