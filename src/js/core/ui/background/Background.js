import Part from './Part';
import LoaderInterface from '../../loader/LoaderInterface';
import { asset } from '../../../config.js';

class Background{
	constructor(ctx, canvas){
		this.ctx = ctx;
		this.container = {
			width: canvas.width,
			height: canvas.height
		}

		this.floor = this.container.height-90;

		this.baseStage = 'stage';
		this.actualStage = 0;
		this.changingstage = false;

		this.stages = {
			stage1: [],
			stage2: [],
			stage3: [],
			stage4: []
		};

		this.parts = [
			'0.png',
			'1.png',
			'2.png',
			'3.png'
		];

		for(var stage in this.stages){
			this.parts.forEach(function(part, index){
				var path = asset.build('/stages/'+stage+'/parts/'+part);
				this.stages[stage].push(new Part(ctx, path, {
					speed: index+1*2
				}));
			}.bind(this))
		}
		this.nextStage();

		this.obstacles = {
			max: 2,
			content: []
		};

		this.enemies = {
			max: 2,
			content: []
		};
	}

	addObstacle(){
		if(this.obstacles.content.length < this.obstacles.max){
			var rand = Math.floor(Math.random() * 500) === 250;
			if(rand){
				var obs = {
					img: new Image(),
					left: this.container.width
				}
				var randObs = Math.floor(Math.random() * 0);
				obs.img.src = asset.build('/stages/'+this.stage+'/obstacles/'+randObs+'.png');
				this.obstacles.content.push(obs);
			}
		}
	}

	addEnemy(){
		if(this.enemies.content.length < this.enemies.max){
			var rand = Math.floor(Math.random() * 500) === 250;
			if(rand){
				var obs = {
					img: new Image(),
					left: this.container.width
				}
				var randEnemy = Math.floor(Math.random() * 0);
				obs.img.src = asset.build('/stages/'+this.stage+'/enemies/'+randEnemy+'.png');
				this.enemies.content.push(obs);
			}
		}
	}

	nextStage(label){
		this.actualStage++;
		this.stage = label || this.baseStage + this.actualStage;
		this.floorPart = this.stages[this.stage].length-1;
	}

	loader(){
		var ressources = [];
		for(var stage in this.stages){
			ressources = ressources.concat(this.stages[stage].map(function(part){return new LoaderInterface(part)}))
		}
		return ressources;
	}

	move(){
		this.stages[this.stage].forEach(function(part, index){
			this.addObstacle();
			this.addEnemy();

			var move =  part.speed*this.speed;

			if(part.img.width - part.left - part.speed < 0){ 
				part.left = move;
			}
			else{
				part.left += move;
			}
			
			if(part.img.width - part.left < this.container.width){
				var stick = part.img.width - part.left;
				this.ctx.drawImage(part.img, 0, 0, this.container.width - stick, this.container.height, stick, 0, this.container.width - stick, this.container.height)
				this.ctx.drawImage(part.img, part.left, 0, stick, this.container.height, 0, 0, stick, this.container.height)
			}
			else{
				this.ctx.drawImage(part.img, part.left, 0, this.container.width, this.container.height, 0, 0, this.container.width, this.container.height)
			}

			if(index == this.floorPart){
				this.obstacles.content.forEach(function(obs){
					this.ctx.drawImage(obs.img, obs.left, this.floor - obs.img.naturalHeight)
					obs.left -= move;
					if(obs.left < 0 - obs.img.naturalWidth){
						this.obstacles.content.shift();
						if(this.obstacles.content.length){
							obs = this.obstacles.content[0];
							this.ctx.drawImage(obs.img, obs.left, this.floor - obs.img.naturalHeight)
							obs.left -= move;
						}
					}
				}.bind(this));

				this.enemies.content.forEach(function(enemy){
					this.ctx.drawImage(enemy.img, enemy.left, this.floor - enemy.img.naturalHeight)
					enemy.left -= move + 2 ;
					if(enemy.left < 0 - enemy.img.naturalWidth){
						this.enemies.content.shift();
						if(this.enemies.content.length){
							enemy = this.enemies.content[0];
							this.ctx.drawImage(enemy.img, enemy.left, this.floor - enemy.img.naturalHeight)
							enemy.left -= move + 2;
						}
					}
				}.bind(this));
			}
		}.bind(this))
	}
}

module.exports = Background;