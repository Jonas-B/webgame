import Printer from '../Printer';

class Obstacle extends Printer{
	constructor(ctx, src, options){
		super(ctx);

		options = options || {};

		this.img = new Image();
		
		if(options.onload) this.img.addEventListener('load', options.onload);

		this.label = 'Background Obstacle'
		this.src = src;
		this.ressource = this.img;

		this.left = options.left || 1920;
		this.speed = options.speed || 5;
	}
}