import Printer from '../Printer';

class Part extends Printer{
	constructor(ctx, src, options){
		super(ctx);

		options = options || {};
		this.img = new Image();
		
		if(options.onload) this.img.addEventListener('load', options.onload);

		this.label = 'Background Part'
		this.src = src;
		this.ressource = this.img;

		this.left = 0;
		this.speed = options.speed || 5;
	}
}

module.exports = Part;