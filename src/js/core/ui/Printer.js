import LoaderInterface from '../loader/LoaderInterface'

class Printer{
	constructor(ctx){
		this.ctx = ctx;
	}

	loader(){
		return new LoaderInterface(this);
	}
}

module.exports = Printer;