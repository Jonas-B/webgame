class Keyboard{
	constructor(){
		this.events = {
			38: 'jump',
			40: 'sneak'
		}
	}

	handleKey(e){
		if(e.keyCode){
			return this.events[e.keyCode];
		}
	}
}

module.exports = Keyboard;