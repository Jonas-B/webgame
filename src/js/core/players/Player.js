import Printer from '../ui/player/Player.js';

class Player extends Printer{
	constructor(ctx){
		super(ctx)
		this.name = "Unknown name";
		this.description = "Unknown description";
		this.picked = "Good choice !";
		this.hp = 1;
	}

	getInfos(){
		return {
			img: this.src,
			name: this.name,
			description: this.description,
			picked: this.picked,
			speed: this.speed,
			hp: this.hp,
			jump: this.jumpHight
		}
	}
}

module.exports = Player;