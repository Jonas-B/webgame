import Player from './Player.js';
import config from '../../config.js';

class Link extends Player{
	constructor(ctx){
		super(ctx);
		this.name = "Link";
		this.description = "Best description ever";

		this.width = 120;
		this.height = 130;

		this.states.run.target = 910;
		this.states.jump.target = 780;
		this.states.jump.time = 1;
		this.states.sneak.target = 650;

		this.speed = 15;
		this.jumpHight = 200;
		this.src = config.asset.build() + 'images/chars/linkv2.png';
	}
}

module.exports = Link;