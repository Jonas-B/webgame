import Player from './Player.js';
import config from '../../config.js';

class Zelda extends Player{
	constructor(ctx){
		super(ctx);
		this.name = "Zelda";
		this.description = "I'm Zelda ... Graouuw...";
		this.picked = "I'M THE REAL ONE";

		this.width = 188;
		this.height = 240;

		this.states.run.target = 0;
		this.states.jump.target = 245;
		this.states.jump.time = 1;
		this.states.sneak.target = 240;

		this.speed = 30;
		this.jumpHight = 100;
		this.src = config.asset.build() + 'images/chars/zelda.png';
	}
}

module.exports = Zelda;