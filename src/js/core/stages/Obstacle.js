import LoaderInterface from '../loader/LoaderInterface'

class Obstacle{
	constructor(stage){
		this.img = new Image();
	}

	loader(){
		return new LoaderInterface(this);
	}
}