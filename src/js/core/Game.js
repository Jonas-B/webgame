import Player from './ui/player/Player'
import Background from './ui/background/Background'
import Ressources from './loader/Ressources'
import Keyboard from './input/Keyboard'

class Game{
	constructor(cv){
		this.canvas = cv || document.querySelector('#game');
		this.ctx = this.canvas.getContext('2d');
		this.wrap = {
			points: document.querySelector('#points')
		}
		this.frames = 30;
		this.ressources = [];
		this.points = 0;
	}

	init(player, cb){
		this.bg = new Background(this.ctx, this.canvas);

		this.player = player;
		
		this.ressources = new Ressources();

		var toLoad = [];
		toLoad.push(this.player.loader());
		toLoad = toLoad.concat(this.bg.loader());

		this.inputs = new Keyboard();
		document.addEventListener('keydown', function(e){
			this.setState(this.inputs.handleKey(e));
		}.bind(this));

		document.addEventListener('keyup', function(e){
			this.clearState(this.inputs.handleKey(e));
		}.bind(this));

		console.log(toLoad)
		this.ressources.loadAll(toLoad, function(){
			this.player.y = this.bg.floor-this.player.height;
			this.player.basey = this.player.y;
			if(cb) cb();
		}.bind(this))

		this.bg.speed = this.player.speed/15;
	}

	print(){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.bg.move();
 		this.player.move();
 		console.log(this.ctx, this.player.ctx);

 		this.points += this.bg.stages[this.bg.stage][this.bg.floorPart].speed;
 		this.wrap.points.innerHTML = this.points;
	}

	setState(state){
		if(state){
			this.player.updateState(state);
		}
	}

	clearState(state){
		if(state){
			if(state == 'sneak'){
				this.player.updateState(this.player.defaultState);
			}
		}
	}

	run(){
		setInterval(this.print.bind(this), 1000/this.frames);
	}

	getPlayers(playersData){
		var characters = [];
		playersData.forEach(function(charData){
			characters.push(new Player(this.ctx, charData));
		}.bind(this));
		return characters;
	}
}

module.exports = Game;