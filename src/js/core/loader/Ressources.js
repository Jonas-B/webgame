class Ressources{
	constructor(){
		this.done = true;
	}
	load(loaderInterface, cb){
		loaderInterface.ressource.addEventListener('load', function(){
			console.log(loaderInterface.src, 'loaded.');
			if(cb) cb();
		})
		loaderInterface.ressource.addEventListener('error', function(){
			console.log(loaderInterface.src, 'error.')
			if(cb) cb();
		})
		loaderInterface.ressource.src = loaderInterface.src;
	}

	loadAll(loadersInterface, cb){
		if(this.done){
			this.done = false;
			this.time = Date.now();
		}
		if(loadersInterface.length){
			var loaderInterface = loadersInterface.shift();
			this.load(loaderInterface, function(){
				this.loadAll(loadersInterface, cb);
			}.bind(this))
		}
		else{
			this.done = true;
			var now = Date.now();
			console.log('Resources import finished, ', now - this.time, 'ms');
			
			cb();
		}
	}
}

module.exports = Ressources;