class LoaderInterface{
	constructor(data){
		// Default values
		var defaultValues = {
			label : 'Undefined',
			src : 'path/to/unfounded/ressource',
			ressource : null
		}

		// Pull value from passed data
		this.label = data.label || defaultValues.label;
		this.src = data.src || defaultValues.src;
		this.ressource = data.ressource || defaultValues.ressource;

		// Feedback to dev / user
		if(!this.label || !this.src || !this.ressource){
			console.log('Unable to initialize a ressource.', this)
		}
	}
}

module.exports = LoaderInterface;