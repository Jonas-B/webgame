function ajax(opt) {
	var xmlhttp = new XMLHttpRequest();

	var defaultOpt = {
		url: '/',
		type: 'GET',
		response: 'raw',
		cb: function(){}
	}

	for(var key in defaultOpt){
		opt[key] = opt[key] || defaultOpt[key];
	}

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
			if(opt.cb){
				if(opt.response == "json"){
					opt.cb(JSON.parse(xmlhttp.responseText));
				}
				else{
					opt.cb(xmlhttp);
				}
			}
		}
	}

	xmlhttp.open(opt.type, opt.url, true);
	xmlhttp.send();
}

module.exports = ajax;