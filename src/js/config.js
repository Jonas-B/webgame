module.exports = {
	asset: {
		path: './data/',

		build: function(path){
			if(this.path[this.path.length-1] != '/'){
				this.path = this.path + '/';
			}

			if(path[0] == '/'){
				path = path.substring(1);
			}

			return this.path + path;
		}
	}
}