module.exports = {
    entry: [
        "./src/js/main.js"
    ],
    output: {
        path: './public/js/',
        filename: "bundle.js",
        sourceMapFilename: "[file].map"
    },
    module: {
	  loaders: [
	    {
	     test: /\.js$/,
	     exclude: /node_modules/,
	      loader: 'babel-loader'
	    }
	  ]
	}
};