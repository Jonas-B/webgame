/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!******************!*\
  !*** multi main ***!
  \******************/
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(/*! ./src/js/main.js */1);


/***/ },
/* 1 */
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _config = __webpack_require__(/*! ./config */ 2);
	
	var _ajax = __webpack_require__(/*! ./core/loader/ajax */ 3);
	
	var _ajax2 = _interopRequireDefault(_ajax);
	
	var _Game = __webpack_require__(/*! ./core/Game */ 4);
	
	var _Game2 = _interopRequireDefault(_Game);
	
	var _playerCard = __webpack_require__(/*! ./select/player-card */ 12);
	
	var _playerCard2 = _interopRequireDefault(_playerCard);
	
	var _Player = __webpack_require__(/*! ./core/ui/player/Player */ 5);
	
	var _Player2 = _interopRequireDefault(_Player);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	NodeList.prototype.forEach = Array.prototype.forEach;
	
	var container = document.querySelector('.container');
	var gamecontainer = document.querySelector('.game');
	var cv = document.querySelector('#game-canvas');
	var ctx = cv.getContext('2d');
	var game = new _Game2.default(cv);
	var selector = document.querySelector('.player-select');
	
	var printCards = function printCards(chars) {
		var cards = [];
		var bestCaract = {
			hp: 0,
			speed: 0,
			jump: 0
		};
	
		chars.forEach(function (char) {
			var card = new _playerCard2.default(char);
			selector.appendChild(card.container);
			cards.push(card);
	
			for (var carac in bestCaract) {
				if (bestCaract[carac] < char[carac]) {
					bestCaract[carac] = char[carac];
				}
			}
		});
	
		cards.forEach(function (card) {
			card.propStrength(bestCaract);
		});
	};
	
	(0, _ajax2.default)({
		url: _config.asset.build('/chars/chars.json'),
		response: 'json',
		cb: function cb(data) {
			var characters = game.getPlayers(data);
	
			printCards(characters);
	
			var menu = document.querySelector('.menu .hamburger');
			var options = document.querySelector('.menu .options');
			menu.addEventListener('click', function () {
				menu.classList.toggle('is-active');
				options.classList.toggle('opened');
			});
	
			var windowHeight = window.innerHeight;
			var windowWidth = window.innerWidth;
			var windowMax = windowWidth < windowHeight ? windowHeight : windowWidth;
	
			var cards = document.querySelectorAll('.player-card');
			cards.forEach(function (card, cardIndex) {
				card.addEventListener('click', function (e) {
					var selected = document.querySelector('.player-card.selected');
					if (selected) selected.classList.remove('selected');
					card.classList.add('selected');
				});
	
				var playBtn = card.querySelector('.play');
				playBtn.addEventListener('click', function (e) {
					var x = e.pageX;
					var y = e.pageY;
	
					var t = document.createElement('div');
					t.className = 'game-transition';
					t.style.left = x + 'px';
					t.style.top = y + 'px';
	
					var m = document.createElement('div');
					m.className = 'game-transition-msg';
					m.innerHTML = '<p><h1> You picked <strong>' + card.querySelector('.name').textContent + '</strong> </h1></p><p>' + card.querySelector('.picked').textContent + '</p>';
					t.append(m);
	
					document.body.append(t);
					setTimeout(function () {
						t.style.left = '-' + windowMax / 2 + 'px';
						t.style.width = 2 * windowMax + 'px';
	
						t.style.top = '-' + windowMax / 2 + 'px';
						t.style.height = 2 * windowMax + 'px';
	
						setTimeout(function () {
							container.style.display = 'none';
							gamecontainer.style.display = 'flex';
							cv.height = 1080;
							cv.width = 1920;
	
							game.init(characters[cardIndex], function () {
								game.print();
								t.style.opacity = '0';
								game.run();
							});
						}, 1000);
					}, 10);
				});
			});
		}
	});

/***/ },
/* 2 */
/*!**************************!*\
  !*** ./src/js/config.js ***!
  \**************************/
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
		asset: {
			path: './data/',
	
			build: function build(path) {
				if (this.path[this.path.length - 1] != '/') {
					this.path = this.path + '/';
				}
	
				if (path[0] == '/') {
					path = path.substring(1);
				}
	
				return this.path + path;
			}
		}
	};

/***/ },
/* 3 */
/*!************************************!*\
  !*** ./src/js/core/loader/ajax.js ***!
  \************************************/
/***/ function(module, exports) {

	'use strict';
	
	function ajax(opt) {
		var xmlhttp = new XMLHttpRequest();
	
		var defaultOpt = {
			url: '/',
			type: 'GET',
			response: 'raw',
			cb: function cb() {}
		};
	
		for (var key in defaultOpt) {
			opt[key] = opt[key] || defaultOpt[key];
		}
	
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				if (opt.cb) {
					if (opt.response == "json") {
						opt.cb(JSON.parse(xmlhttp.responseText));
					} else {
						opt.cb(xmlhttp);
					}
				}
			}
		};
	
		xmlhttp.open(opt.type, opt.url, true);
		xmlhttp.send();
	}
	
	module.exports = ajax;

/***/ },
/* 4 */
/*!*****************************!*\
  !*** ./src/js/core/Game.js ***!
  \*****************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Player = __webpack_require__(/*! ./ui/player/Player */ 5);
	
	var _Player2 = _interopRequireDefault(_Player);
	
	var _Background = __webpack_require__(/*! ./ui/background/Background */ 8);
	
	var _Background2 = _interopRequireDefault(_Background);
	
	var _Ressources = __webpack_require__(/*! ./loader/Ressources */ 10);
	
	var _Ressources2 = _interopRequireDefault(_Ressources);
	
	var _Keyboard = __webpack_require__(/*! ./input/Keyboard */ 11);
	
	var _Keyboard2 = _interopRequireDefault(_Keyboard);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Game = function () {
		function Game(cv) {
			_classCallCheck(this, Game);
	
			this.canvas = cv || document.querySelector('#game');
			this.ctx = this.canvas.getContext('2d');
			this.wrap = {
				points: document.querySelector('#points')
			};
			this.frames = 30;
			this.ressources = [];
			this.points = 0;
		}
	
		_createClass(Game, [{
			key: 'init',
			value: function init(player, cb) {
				this.bg = new _Background2.default(this.ctx, this.canvas);
	
				this.player = player;
	
				this.ressources = new _Ressources2.default();
	
				var toLoad = [];
				toLoad.push(this.player.loader());
				toLoad = toLoad.concat(this.bg.loader());
	
				this.inputs = new _Keyboard2.default();
				document.addEventListener('keydown', function (e) {
					this.setState(this.inputs.handleKey(e));
				}.bind(this));
	
				document.addEventListener('keyup', function (e) {
					this.clearState(this.inputs.handleKey(e));
				}.bind(this));
	
				console.log(toLoad);
				this.ressources.loadAll(toLoad, function () {
					this.player.y = this.bg.floor - this.player.height;
					this.player.basey = this.player.y;
					if (cb) cb();
				}.bind(this));
	
				this.bg.speed = this.player.speed / 15;
			}
		}, {
			key: 'print',
			value: function print() {
				this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
				this.bg.move();
				this.player.move();
				console.log(this.ctx, this.player.ctx);
	
				this.points += this.bg.stages[this.bg.stage][this.bg.floorPart].speed;
				this.wrap.points.innerHTML = this.points;
			}
		}, {
			key: 'setState',
			value: function setState(state) {
				if (state) {
					this.player.updateState(state);
				}
			}
		}, {
			key: 'clearState',
			value: function clearState(state) {
				if (state) {
					if (state == 'sneak') {
						this.player.updateState(this.player.defaultState);
					}
				}
			}
		}, {
			key: 'run',
			value: function run() {
				setInterval(this.print.bind(this), 1000 / this.frames);
			}
		}, {
			key: 'getPlayers',
			value: function getPlayers(playersData) {
				var characters = [];
				playersData.forEach(function (charData) {
					characters.push(new _Player2.default(this.ctx, charData));
				}.bind(this));
				return characters;
			}
		}]);
	
		return Game;
	}();
	
	module.exports = Game;

/***/ },
/* 5 */
/*!*****************************************!*\
  !*** ./src/js/core/ui/player/Player.js ***!
  \*****************************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _config = __webpack_require__(/*! ../../../config */ 2);
	
	var _Printer2 = __webpack_require__(/*! ../Printer */ 6);
	
	var _Printer3 = _interopRequireDefault(_Printer2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Player = function (_Printer) {
		_inherits(Player, _Printer);
	
		function Player(ctx) {
			var opt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
			_classCallCheck(this, Player);
	
			var _this = _possibleConstructorReturn(this, (Player.__proto__ || Object.getPrototypeOf(Player)).call(this, ctx));
	
			_this.default = {
				name: "Unknown name",
				description: "Unknown description",
				picked: "Good choice !",
				hp: 1,
	
				x: 50,
				y: 200,
	
				label: 'Avatar',
	
				width: 0,
				height: 0,
	
				speed: 0,
				jump: 50,
	
				src: 'chars/img/undefined.png'
			};
	
			for (var key in _this.default) {
				_this[key] = opt[key] || _this.default[key];
			}
	
			_this.src = _config.asset.build(_this.src);
	
			_this.jumpTime = 1;
			_this.basey = _this.y;
			_this.moving = false;
	
			_this.defaultState = "run";
			_this.state = {
				default: "run",
				current: "run",
				locked: false
			};
	
			opt.states = opt.states || {};
			opt.states.run = opt.states.run || {};
			opt.states.jump = opt.states.jump || {};
			opt.states.sneak = opt.states.sneak || {};
	
			_this.states = {
				run: {
					target: opt.states.run.target || 0,
					frame: 0,
					step: 0
				},
				jump: {
					target: opt.states.jump.target || 0,
					frame: 0,
					step: 0,
					time: 0
				},
				sneak: {
					target: opt.states.sneak.target || 0,
					frame: 0,
					step: 0
				}
			};
	
			_this.img = new Image();
			_this.ressource = _this.img;
	
			console.log(_this.ctx);
			return _this;
		}
	
		_createClass(Player, [{
			key: 'frames',
			value: function frames() {
				return Math.round(this.img.width / this.width) - 1;
			}
		}, {
			key: 'move',
			value: function move() {
				var state = this.state.current;
				var stateData = this.states[state];
	
				this.ctx.drawImage(this.img, stateData.frame, stateData.target, this.width, this.height, this.x, this.y, this.width, this.height);
	
				if (!this.moving) {
					if (this.state.locked) {
						if (this.state.current == 'jump') {
							if (stateData.step == this.frames()) {
								this.state.locked = false;
							}
						} else {
							this.state.locked = false;
						}
					}
					if (this.state.current == 'jump') {
						var jumpSteps = this.speed * this.jumpTime;
						var half = jumpSteps / 2;
						var mult = Math.sqrt(this.jump) / half;
						var xstep = (stateData.time - half) * mult;
						var jumpRatio = xstep * -xstep + this.jump;
						this.y = this.basey - jumpRatio;
						stateData.time += 1;
						var tick = jumpSteps / this.frames();
						stateData.step += stateData.time % tick == 0 ? 1 : 0;
					} else {
						stateData.step += 1;
					}
	
					this.moving = true;
					this.reset = setTimeout(function () {
						stateData.frame = stateData.step * this.width;
						if (this.state.current == state) {
							if (stateData.frame >= this.img.width) {
								stateData.frame = 0;
								stateData.step = 0;
								stateData.time = 0;
								if (this.state.current == 'jump') {
									this.state.current = this.state.default;
								}
							}
						}
						this.moving = false;
					}.bind(this), 1000 / this.speed);
				}
			}
		}, {
			key: 'updateState',
			value: function updateState(state) {
				if (this.state.current != state) {
					if (!this.state.locked) {
						console.log('State update.', state);
	
						this.states[this.state.current].step = 0;
						this.states[this.state.current].frame = 0;
	
						this.state.current = state;
						this.state.locked = true;
	
						this.states[state].step = 0;
						this.states[state].frame = 0;
					}
				}
			}
		}]);
	
		return Player;
	}(_Printer3.default);
	
	module.exports = Player;

/***/ },
/* 6 */
/*!***********************************!*\
  !*** ./src/js/core/ui/Printer.js ***!
  \***********************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _LoaderInterface = __webpack_require__(/*! ../loader/LoaderInterface */ 7);
	
	var _LoaderInterface2 = _interopRequireDefault(_LoaderInterface);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Printer = function () {
		function Printer(ctx) {
			_classCallCheck(this, Printer);
	
			this.ctx = ctx;
		}
	
		_createClass(Printer, [{
			key: 'loader',
			value: function loader() {
				return new _LoaderInterface2.default(this);
			}
		}]);
	
		return Printer;
	}();
	
	module.exports = Printer;

/***/ },
/* 7 */
/*!***********************************************!*\
  !*** ./src/js/core/loader/LoaderInterface.js ***!
  \***********************************************/
/***/ function(module, exports) {

	'use strict';
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var LoaderInterface = function LoaderInterface(data) {
		_classCallCheck(this, LoaderInterface);
	
		// Default values
		var defaultValues = {
			label: 'Undefined',
			src: 'path/to/unfounded/ressource',
			ressource: null
		};
	
		// Pull value from passed data
		this.label = data.label || defaultValues.label;
		this.src = data.src || defaultValues.src;
		this.ressource = data.ressource || defaultValues.ressource;
	
		// Feedback to dev / user
		if (!this.label || !this.src || !this.ressource) {
			console.log('Unable to initialize a ressource.', this);
		}
	};
	
	module.exports = LoaderInterface;

/***/ },
/* 8 */
/*!*************************************************!*\
  !*** ./src/js/core/ui/background/Background.js ***!
  \*************************************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Part = __webpack_require__(/*! ./Part */ 9);
	
	var _Part2 = _interopRequireDefault(_Part);
	
	var _LoaderInterface = __webpack_require__(/*! ../../loader/LoaderInterface */ 7);
	
	var _LoaderInterface2 = _interopRequireDefault(_LoaderInterface);
	
	var _config = __webpack_require__(/*! ../../../config.js */ 2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Background = function () {
		function Background(ctx, canvas) {
			_classCallCheck(this, Background);
	
			this.ctx = ctx;
			this.container = {
				width: canvas.width,
				height: canvas.height
			};
	
			this.floor = this.container.height - 90;
	
			this.baseStage = 'stage';
			this.actualStage = 0;
			this.changingstage = false;
	
			this.stages = {
				stage1: [],
				stage2: [],
				stage3: [],
				stage4: []
			};
	
			this.parts = ['0.png', '1.png', '2.png', '3.png'];
	
			for (var stage in this.stages) {
				this.parts.forEach(function (part, index) {
					var path = _config.asset.build('/stages/' + stage + '/parts/' + part);
					this.stages[stage].push(new _Part2.default(ctx, path, {
						speed: index + 1 * 2
					}));
				}.bind(this));
			}
			this.nextStage();
	
			this.obstacles = {
				max: 2,
				content: []
			};
	
			this.enemies = {
				max: 2,
				content: []
			};
		}
	
		_createClass(Background, [{
			key: 'addObstacle',
			value: function addObstacle() {
				if (this.obstacles.content.length < this.obstacles.max) {
					var rand = Math.floor(Math.random() * 500) === 250;
					if (rand) {
						var obs = {
							img: new Image(),
							left: this.container.width
						};
						var randObs = Math.floor(Math.random() * 0);
						obs.img.src = _config.asset.build('/stages/' + this.stage + '/obstacles/' + randObs + '.png');
						this.obstacles.content.push(obs);
					}
				}
			}
		}, {
			key: 'addEnemy',
			value: function addEnemy() {
				if (this.enemies.content.length < this.enemies.max) {
					var rand = Math.floor(Math.random() * 500) === 250;
					if (rand) {
						var obs = {
							img: new Image(),
							left: this.container.width
						};
						var randEnemy = Math.floor(Math.random() * 0);
						obs.img.src = _config.asset.build('/stages/' + this.stage + '/enemies/' + randEnemy + '.png');
						this.enemies.content.push(obs);
					}
				}
			}
		}, {
			key: 'nextStage',
			value: function nextStage(label) {
				this.actualStage++;
				this.stage = label || this.baseStage + this.actualStage;
				this.floorPart = this.stages[this.stage].length - 1;
			}
		}, {
			key: 'loader',
			value: function loader() {
				var ressources = [];
				for (var stage in this.stages) {
					ressources = ressources.concat(this.stages[stage].map(function (part) {
						return new _LoaderInterface2.default(part);
					}));
				}
				return ressources;
			}
		}, {
			key: 'move',
			value: function move() {
				this.stages[this.stage].forEach(function (part, index) {
					this.addObstacle();
					this.addEnemy();
	
					var move = part.speed * this.speed;
	
					if (part.img.width - part.left - part.speed < 0) {
						part.left = move;
					} else {
						part.left += move;
					}
	
					if (part.img.width - part.left < this.container.width) {
						var stick = part.img.width - part.left;
						this.ctx.drawImage(part.img, 0, 0, this.container.width - stick, this.container.height, stick, 0, this.container.width - stick, this.container.height);
						this.ctx.drawImage(part.img, part.left, 0, stick, this.container.height, 0, 0, stick, this.container.height);
					} else {
						this.ctx.drawImage(part.img, part.left, 0, this.container.width, this.container.height, 0, 0, this.container.width, this.container.height);
					}
	
					if (index == this.floorPart) {
						this.obstacles.content.forEach(function (obs) {
							this.ctx.drawImage(obs.img, obs.left, this.floor - obs.img.naturalHeight);
							obs.left -= move;
							if (obs.left < 0 - obs.img.naturalWidth) {
								this.obstacles.content.shift();
								if (this.obstacles.content.length) {
									obs = this.obstacles.content[0];
									this.ctx.drawImage(obs.img, obs.left, this.floor - obs.img.naturalHeight);
									obs.left -= move;
								}
							}
						}.bind(this));
	
						this.enemies.content.forEach(function (enemy) {
							this.ctx.drawImage(enemy.img, enemy.left, this.floor - enemy.img.naturalHeight);
							enemy.left -= move + 2;
							if (enemy.left < 0 - enemy.img.naturalWidth) {
								this.enemies.content.shift();
								if (this.enemies.content.length) {
									enemy = this.enemies.content[0];
									this.ctx.drawImage(enemy.img, enemy.left, this.floor - enemy.img.naturalHeight);
									enemy.left -= move + 2;
								}
							}
						}.bind(this));
					}
				}.bind(this));
			}
		}]);
	
		return Background;
	}();
	
	module.exports = Background;

/***/ },
/* 9 */
/*!*******************************************!*\
  !*** ./src/js/core/ui/background/Part.js ***!
  \*******************************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _Printer2 = __webpack_require__(/*! ../Printer */ 6);
	
	var _Printer3 = _interopRequireDefault(_Printer2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var Part = function (_Printer) {
		_inherits(Part, _Printer);
	
		function Part(ctx, src, options) {
			_classCallCheck(this, Part);
	
			var _this = _possibleConstructorReturn(this, (Part.__proto__ || Object.getPrototypeOf(Part)).call(this, ctx));
	
			options = options || {};
			_this.img = new Image();
	
			if (options.onload) _this.img.addEventListener('load', options.onload);
	
			_this.label = 'Background Part';
			_this.src = src;
			_this.ressource = _this.img;
	
			_this.left = 0;
			_this.speed = options.speed || 5;
			return _this;
		}
	
		return Part;
	}(_Printer3.default);
	
	module.exports = Part;

/***/ },
/* 10 */
/*!******************************************!*\
  !*** ./src/js/core/loader/Ressources.js ***!
  \******************************************/
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Ressources = function () {
		function Ressources() {
			_classCallCheck(this, Ressources);
	
			this.done = true;
		}
	
		_createClass(Ressources, [{
			key: 'load',
			value: function load(loaderInterface, cb) {
				loaderInterface.ressource.addEventListener('load', function () {
					console.log(loaderInterface.src, 'loaded.');
					if (cb) cb();
				});
				loaderInterface.ressource.addEventListener('error', function () {
					console.log(loaderInterface.src, 'error.');
					if (cb) cb();
				});
				loaderInterface.ressource.src = loaderInterface.src;
			}
		}, {
			key: 'loadAll',
			value: function loadAll(loadersInterface, cb) {
				if (this.done) {
					this.done = false;
					this.time = Date.now();
				}
				if (loadersInterface.length) {
					var loaderInterface = loadersInterface.shift();
					this.load(loaderInterface, function () {
						this.loadAll(loadersInterface, cb);
					}.bind(this));
				} else {
					this.done = true;
					var now = Date.now();
					console.log('Resources import finished, ', now - this.time, 'ms');
	
					cb();
				}
			}
		}]);
	
		return Ressources;
	}();
	
	module.exports = Ressources;

/***/ },
/* 11 */
/*!***************************************!*\
  !*** ./src/js/core/input/Keyboard.js ***!
  \***************************************/
/***/ function(module, exports) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Keyboard = function () {
		function Keyboard() {
			_classCallCheck(this, Keyboard);
	
			this.events = {
				38: 'jump',
				40: 'sneak'
			};
		}
	
		_createClass(Keyboard, [{
			key: 'handleKey',
			value: function handleKey(e) {
				if (e.keyCode) {
					return this.events[e.keyCode];
				}
			}
		}]);
	
		return Keyboard;
	}();
	
	module.exports = Keyboard;

/***/ },
/* 12 */
/*!**************************************!*\
  !*** ./src/js/select/player-card.js ***!
  \**************************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _config = __webpack_require__(/*! ../config */ 2);
	
	var Card = function Card(player) {
		this.props = {};
		this.container = document.createElement('div');
		this.imgWrap = document.createElement('div');
		this.contentWrap = document.createElement('div');
		this.propsWrap = document.createElement('div');
		this.detailWrap = document.createElement('div');
		this.picked = document.createElement('div');
	
		this.img = document.createElement('img');
		this.name = document.createElement('h2');
		this.description = document.createElement('div');
		this.btn = document.createElement('button');
	
		this.imgWrap.appendChild(this.img);
		this.detailWrap.appendChild(this.description);
		this.detailWrap.appendChild(this.btn);
		this.contentWrap.appendChild(this.name);
		this.contentWrap.appendChild(this.propsWrap);
		this.contentWrap.appendChild(this.detailWrap);
		this.container.appendChild(this.imgWrap);
		this.container.appendChild(this.contentWrap);
		this.container.appendChild(this.picked);
	
		this.style = function () {
			this.container.className = 'player-card';
			this.imgWrap.className = 'card-img';
			this.contentWrap.className = 'card-text';
			this.propsWrap.className = 'props';
			this.detailWrap.className = 'detail';
			this.picked.className = 'picked';
	
			this.name.className = 'name';
			this.description.className = 'description';
			this.btn.className = 'play';
		};
	
		this.staticContent = function () {
			this.btn.innerHTML = '<i class="material-icons">play_circle_filled</i> Play !';
			this.name.innerHTML = player.name;
			this.description.innerHTML = player.description;
			this.img.src = player.src;
			this.picked.innerHTML = player.picked;
		};
	
		this.makeProps = function () {
			var props = { hp: "favorite", speed: "arrow_forward", jump: "arrow_upward" };
			for (var prop in props) {
				if (player[prop] != undefined) {
					this.props[prop] = player[prop];
					var propWrap = document.createElement('div');
					propWrap.className = 'prop ' + prop;
	
					var propIcon = document.createElement('div');
					propIcon.className = 'ico material-icons';
					propIcon.innerHTML = props[prop];
	
					var propValue = document.createElement('div');
					propValue.className = 'value';
	
					propWrap.appendChild(propIcon);
					propWrap.appendChild(propValue);
	
					this.propsWrap.appendChild(propWrap);
				}
			}
		};
	
		this.style();
		this.staticContent();
		this.makeProps();
	
		this.propStrength = function (best) {
			for (var prop in best) {
				var node = this.propsWrap.querySelector('.prop.' + prop + ' .value');
				var percent = Math.round(this.props[prop] / best[prop] * 100);
				node.style.width = percent + '%';
			}
		};
	};
	
	module.exports = Card;

/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map