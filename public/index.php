<!DOCTYPE html>
<html>
<head>
	<title>Game</title>

	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="menu">
		<button class="hamburger hamburger--slider" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
		<div class="options">
			<a class="option" href='#' title='Go back to your last visited page'> <i class="material-icons">arrow_back</i> </a>
			<a class="option" href='#' title='Go to Adapti home'> <i class="material-icons">home</i> </a>
			<a class="option" href='#' title='Go to hero selection'> <i class="material-icons">face</i> </a>
			<a class="option" href='#' title='Go to game high scores'> <i class="material-icons">equalizer</i> </a>
		</div>
	</div>
	<div class="container">
		<h1>Pick your fav hero !</h1>
		<div class='player-select'>
		</div>
	</div>

	<div class="game">
		<div id="points"></div>
		<canvas id="game-canvas"></canvas>
	</div>
	<script type="text/javascript" src="js/bundle.js"></script>
</body>
</html>